//
//  ViewController.swift
//  Swiper
//
//  Created by Nathan Geronimo on 7/23/18.
//  Copyright © 2018 Nathan Geronimo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var swipeCount: UITextField!
    var swipeNum: Int = 0 // Keeps track of how many swipes have been done
    var colors = ["white", "red", "orange", "yellow", "green", "blue", "purple"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.swipeCount.text = String(swipeNum) // Sets the value for the number that appears in the middle of the screen
    }
    
    /*
     Forces the status bar on the phone to disappear
     */
    override var prefersStatusBarHidden: Bool {
        return true
    }

    @IBAction func leftSwipe(_ sender: UISwipeGestureRecognizer) {
        updateSwipeCount(count: swipeNum)
        if (0 == (swipeNum % 10)) {
            self.view.backgroundColor = changeColor()
        }
    }
    
    @IBAction func rightSwipe(_ sender: UISwipeGestureRecognizer) {
        updateSwipeCount(count: swipeNum)
        if (0 == (swipeNum % 10)) {
            self.view.backgroundColor = changeColor()
        }
    }
    
    @IBAction func upSwipe(_ sender: UISwipeGestureRecognizer) {
        updateSwipeCount(count: swipeNum)
        if (0 == (swipeNum % 10)) {
            self.view.backgroundColor = changeColor()
        }
    }
    
    @IBAction func downSwipe(_ sender: UISwipeGestureRecognizer) {
        updateSwipeCount(count: swipeNum)
        if (0 == (swipeNum % 10)) {
            self.view.backgroundColor = changeColor()
        }
    }
    
    /*
     * Simply resets the count to 0
     */
    @IBAction func reset(_ sender: Any) {
        swipeNum = 0
        self.swipeCount.text = String(swipeNum)
    }
    
    @IBAction func settings(_ sender: UIButton) {
        
    }
    /*
     * Updates the swipeNum count whenever a swipe is done.
     * Then changes the swipeCount number to the new swipeNum value
     */
    func updateSwipeCount(count: Int) {
        swipeNum += 1
        self.swipeCount.text = String(swipeNum)
    }
    
    /*
     * Randomly picks a number within the range of the colors array.
     */
    func changeColor() -> UIColor {
        let rand = Int.random(in: colors.indices)
        return pickColor(index: rand)
    }
    
    /*
     * Picks a UIColor depending on the number passed in.
     * Returns the UIColor selected
     */
    func pickColor(index: Int) -> UIColor {
        switch index {
        case 1:
            return UIColor.red
        case 2:
            return UIColor.orange
        case 3:
            return UIColor.yellow
        case 4:
            return UIColor.green
        case 5:
            return UIColor.blue
        case 6:
            return UIColor.purple
        default:
            return UIColor.white
        }
    }
}

/**
 TODO:
    - Allow multiple finger gestures and increase the count depending on how many fingers used to swipe.
    - Create options to set the number of swipes that change the background color
      create a settings button too. Include "Reset" button in there?
    - On color change, make the screen transition depending on the swipe that caused the background color change
    - Include funny text boxes that pop up at every "milestone" (e.g. 50, 100, 150, etc.)
 */
